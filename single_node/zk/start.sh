#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/single_node/zk/data:/var/lib/zookeeper/data -v ~/kafka_cluster/single_node/zk/log:/var/lib/zookeeper/log --name=zk zk
