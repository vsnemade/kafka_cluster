How we created .jks files?
openssl req -new -x509 -keyout ca-key -out ca-cert -days 3650

keytool -keystore kafka.server.truststore.jks -alias CA -import -file ca-cert

keytool -keystore kafka.server.keystore.jks -alias kafka-broker -validity 3650 -genkey -keyalg RSA

keytool -keystore kafka.server.keystore.jks -alias kafka-broker -certreq -file ca-request-broker

openssl x509 -req -CA ca-cert -CAkey ca-key -in ca-request-broker -out ca-signed-broker -days 3650 -CAcreateserial

keytool -keystore kafka.server.keystore.jks -alias CA -import -file ca-cert

keytool -keystore kafka.server.keystore.jks -alias kafka-broker -import -file ca-signed-broker
