#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/single_node/kafka/data:/var/lib/kafka/data --name=kafka kafka
