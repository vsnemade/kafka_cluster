#How to add user/group in ldap
We are setting up ldap server with default users and groups in 'scripts/security/ldap_users'

#How to test if its working
docker exec ldap ldapsearch -x -H ldap://localhost -b dc=vishtech,dc=com -D "cn=admin,dc=vishtech,dc=com" -w admin

#How to test user credentials
docker exec ldap ldapwhoami -vvv -h localhost -p 389 -D "cn=MdsAdmin,ou=users,dc=vishtech,dc=com" -x -w cde34rfv
output:
ldap_initialize( ldap://localhost:389 )
dn:cn=MdsAdmin,ou=users,dc=vishtech,dc=com
Result: Success (0)

