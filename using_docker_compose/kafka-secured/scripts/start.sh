#!/bin/sh

docker run -d --net=host --name=zk zk
echo 'zookeeper started, sleeping for 10 seconds'
sleep 10s

docker run -d --net=host --name=kafka -v /c/Users/vishal/vishshare/kafka-logs:/var/kafka-logs kafka
echo 'kafka started'
sleep 10s

docker run -d --net=host --name=ccc ccc
echo 'ccc started'
