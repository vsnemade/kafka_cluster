#!/bin/sh

docker exec kafka /usr/bin/kafka-topics --create --zookeeper 192.168.99.100:2181 --replication-factor 1 --partitions 1 --topic greeting