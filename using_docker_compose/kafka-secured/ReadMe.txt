This project to start secured kafka cluster (kafka, zk, ccc) without using docker-compose file.
We will create Dockerfile for each component and will build image and run it.

Note: Docker-machine has access to '/c/Users/vishal/' so you can do volume mount from this directory to docker container


#Zookeeper: 
A)Create image:
cmd:cd /e/workspace/kafka-cluster/kafka-secured/zookeeper
cmd:docker build -f Dockerfile -t zk .
B)Run zk
cmd:docker run -d --net=host --name=zk zk

#Kafka: 
A)Create image:
cmd:cd /e/workspace/kafka-cluster/kafka-secured/kafka
cmd:docker build -f Dockerfile -t kafka .
B)Run kafka with bind mount so that we can have window's directory within container.
cmd:docker run -d --net=host --name=kafka -v /c/Users/vishal/vishshare/kafka-logs:/var/kafka-logs kafka

#Create topic 'greeting'
docker exec kafka /usr/bin/kafka-topics --create --zookeeper 192.168.99.100:2181 --replication-factor 1 --partitions 2 --topic greeting

#Describe topic
docker exec kafka /usr/bin/kafka-topics --describe --zookeeper 192.168.99.100:2181 --topic greeting


#cccA)Create Image:
cmd:cd /e/workspace/kafka-cluster/kafka-secured/ccc
cmd:docker build -f Dockerfile -t ccc .
B)Run ccc
cmd:docker run -d --net=host --name=ccc ccc
