#!/bin/sh
#---------------------------------------------------------------------------------------------------------------------------------------
### Consumer ########################################################
keytool -keystore kafka.parthconsumer.truststore.jks -alias CA -import -file ca-cert
keytool -keystore kafka.parthconsumer.keystore.jks -alias parthconsumer -validity 3650 -genkey -keyalg RSA
#Use First Name and Last Name:parthconsumer
keytool -keystore kafka.parthconsumer.keystore.jks -alias parthconsumer -certreq -file ca-request-parthconsumer
openssl x509 -req -CA ca-cert -CAkey ca-key -in ca-request-parthconsumer -out ca-signed-parthconsumer -days 3650 -CAcreateserial
keytool -keystore kafka.parthconsumer.keystore.jks -alias CA -import -file ca-cert
keytool -keystore kafka.parthconsumer.keystore.jks -alias parthconsumer -import -file ca-signed-parthconsumer
#---------------------------------------------------------------------------------------------------------------------------------------

 
