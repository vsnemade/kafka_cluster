#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/multi_node/zk/node3/kafka/log:/var/log/kafka -v ~/kafka_cluster/multi_node/zk/node3/data:/var/lib/zookeeper/data -v ~/kafka_cluster/multi_node/zk/node3/log:/var/lib/zookeeper/log --name=zk3 zk3
