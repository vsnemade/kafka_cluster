#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/multi_node/zk/node1/kafka/log:/var/log/kafka -v ~/kafka_cluster/multi_node/zk/node1/data:/var/lib/zookeeper/data -v ~/kafka_cluster/multi_node/zk/node1/log:/var/lib/zookeeper/log --name=zk1 zk1
