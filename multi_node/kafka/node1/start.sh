#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/multi_node/kafka/node1/data:/var/lib/kafka/data -v ~/kafka_cluster/multi_node/kafka/node1/log:/var/log/kafka --name=kafka1 kafka1
