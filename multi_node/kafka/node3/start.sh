#!/bin/sh

/usr/bin/docker run --net=host -d -v ~/kafka_cluster/multi_node/kafka/node3/data:/var/lib/kafka/data -v ~/kafka_cluster/multi_node/kafka/node3/log:/var/log/kafka --name=kafka3 kafka3
