package com.vishtech.app;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.StringSerializer;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.UUID;

public class KafkaProducer {

    private static final String TOPIC = "transactions";

    //Bypassing the SSL verification to execute our code successfully
    static {
        //disableSSLVerification();
    }

    public static void main(String[] args) {
        produceUsingSchema();
      // produceWithoutSchema();
    }

    private static void produceUsingSchema(){
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "https://localhost:8081");
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,"SSL");
        props.put(SslConfigs.DEFAULT_SSL_ENDPOINT_IDENTIFICATION_ALGORITHM,"");
        //props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "kafka.vishproducer.keystore.jks"); //No LDAP
        //props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "kafka.vishproducer.truststore.jks"); //No LDAP
        props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "kafka.parthproducer.keystore.jks");  //LDAP user
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "kafka.parthproducer.truststore.jks"); //LDAP user
        //props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "schemaregistry.keystore.jks");  //No LDAP user
        //props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "schemaregistry.truststore.jks"); //No LDAP user
        props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG,"cde34rfv");
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "cde34rfv");
        props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "cde34rfv");

        try (org.apache.kafka.clients.producer.KafkaProducer<String, Payment> producer = new org.apache.kafka.clients.producer.KafkaProducer<String, Payment>(props)) {

            for (long i = 0; i < 1; i++) {
                final String orderId = "id-" + Long.toString(i);
                final Payment payment = new Payment(orderId, 1000.00d);
                final ProducerRecord<String, Payment> record = new ProducerRecord<String, Payment>(TOPIC, payment.getId().toString(), payment);
                System.out.println("Sending Payment:" +payment);
                producer.send(record);
                System.out.println("Sent Payment");
              //  Thread.sleep(1000L);
            }

            producer.flush();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private static void produceWithoutSchema(){

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,"SSL");
        props.put(SslConfigs.DEFAULT_SSL_ENDPOINT_IDENTIFICATION_ALGORITHM,"");
        //props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "kafka.vishproducer.keystore.jks"); //No LDAP
        //props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "kafka.vishproducer.truststore.jks"); //No LDAP
        props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "kafka.parthproducer.keystore.jks");  //LDAP user
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "kafka.parthproducer.truststore.jks"); //LDAP user
        props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG,"cde34rfv");
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "cde34rfv");
        props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "cde34rfv");

        Producer<String, String> producer = new org.apache.kafka.clients.producer.KafkaProducer<String, String>(props);
        for(int i = 0; i < 5; i++){
            producer.send(new ProducerRecord<String, String>("demo_topic", "Message-"+ UUID.randomUUID().toString()));
            System.out.println("Sent data");
        }
        producer.close();
        System.out.println("Producer done");
    }

    //Method used for bypassing SSL verification
    public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }

        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

}
