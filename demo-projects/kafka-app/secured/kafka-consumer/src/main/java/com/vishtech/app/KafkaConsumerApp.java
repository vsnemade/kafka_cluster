package  com.vishtech.app;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

public class KafkaConsumerApp {

    public static final String TOPIC = "transactions";

    //Bypassing the SSL verification to execute our code successfully
    static {
        disableSSLVerification();
    }

    public static void main(String[] args) {
       //consumeUsingSchema();
        consumeWithoutSchema();
    }
    public static void consumeUsingSchema() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "vish-grp-2");
        //props.put("enable.auto.commit", "true"); //Automatic Offset Committing
        props.put("enable.auto.commit", "false"); //Manual Offset Control
        //props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "6000");
        props.put("request.timeout.ms", "10000");
        props.put("max.poll.interval.ms", "5000");

        props.put("auto.offset.reset","earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,"SSL");
        props.put(SslConfigs.DEFAULT_SSL_ENDPOINT_IDENTIFICATION_ALGORITHM,"");
        props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "https://localhost:8081");
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        //props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.vishconsumer.keystore.jks");  //No LDAP
        //props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.vishconsumer.truststore.jks");  //No LDAP
        props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.parthconsumer.keystore.jks");  // LDAP User
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.parthconsumer.truststore.jks");  //Ldap User
        props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG,"cde34rfv");
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "cde34rfv");
        props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "cde34rfv");

        try (final KafkaConsumer<String, Payment> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(Collections.singletonList(TOPIC));
            while (true) {
                final ConsumerRecords<String, Payment> records = consumer.poll(Duration.ofMillis(100));
                for (final ConsumerRecord<String, Payment> record : records) {
                    final String key = record.key();
                    final Payment value = record.value();
                    System.out.printf("key = %s, value = %s%n", key, value);
                }
            }

        }



    }

    public static void consumeWithoutSchema() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "vish-grp-1");
        //props.put("enable.auto.commit", "true"); //Automatic Offset Committing
        props.put("enable.auto.commit", "false"); //Manual Offset Control
        //props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "6000");
        props.put("request.timeout.ms", "10000");
        props.put("max.poll.interval.ms", "5000");

        props.put("auto.offset.reset","earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,"SSL");
        props.put(SslConfigs.DEFAULT_SSL_ENDPOINT_IDENTIFICATION_ALGORITHM,"");
        //props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/home/vish/IdeaProjects/kafka-app/kafka-consumer/kafka.vishconsumer.keystore.jks");  //No LDAP
        //props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/home/vish/IdeaProjects/kafka-app/kafka-consumer/kafka.vishconsumer.truststore.jks");  //No LDAP
        props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.parthconsumer.keystore.jks");  // LDAP User
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, "/home/vish/kafka_cluster/demo-projects/kafka-app/secured/kafka-consumer/kafka.parthconsumer.truststore.jks");  //Ldap Userprops.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG,"cde34rfv");
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "cde34rfv");
        props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "cde34rfv");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList("demo_topic"));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            int count = records.count();
           // System.out.println("Received "+records.count() +" messages");
            for (ConsumerRecord<String, String> record : records)
               System.out.println("Consumed: "+record.value());
            if(count>0)
           {
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                //System.out.printf("offset = %d, key = %s, value = %s", record.offset(), record.key(), record.value());
                consumer.commitAsync(); //will block until the offsets have been successfully committed or fatal error has happened during the commit process
                //consumer.commitAsync(); // is non-blocking and will trigger OffsetCommitCallback upon either successfully committed or fatally failed.
                System.out.println("Offset Commited");
                //System.exit(0);
            }
        }

    }

    //Method used for bypassing SSL verification
    public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }

        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

}
