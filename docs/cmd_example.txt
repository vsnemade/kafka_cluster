#How to create topic?
Using Zookeeper:
docker exec zk1 /usr/bin/kafka-topics --create --zookeeper localhost:2181/kafka-dev --replication-factor 3 --partitions 3 --topic demo_topic

Using Kafka:
docker exec kafka1 /usr/bin/kafka-topics --topic first_topic --create --bootstrap-server localhost:9092 --partitions 1 --replication-factor 1


#How to produce?
root@vish-lp1:/usr/bin# ./kafka-console-producer --topic first_topic --broker-list localhost:9092

#How to consume?
root@vish-lp1:/usr/bin# ./kafka-console-consumer --topic first_topic -bootstrap-server localhost:9092

#How to list topics in zookeeper?
docker exec zk1 /usr/bin/kafka-topics --zookeeper localhost:2181/kafka-dev --list

#How to list acls in zookeeper?
docker exec zk1 /usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --list

#How to add acl for topic in zookeeper?
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --add --allow-principal User:client  --operation Write  --topic 'demo_topic' 
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --add --allow-principal User:client  --operation Read  --topic 'demo_topic' 
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --add --allow-principal User:client  --operation Describe  --topic 'demo_topic' 

#How to add acl for consumer group in zookeeper?
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --add --allow-principal User:client  --operation Read  --group 'vish-grp-1' 
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --add --allow-principal User:client  --operation Describe   --group 'vish-grp-1' 


#How to remove acl for topic in zookeeper?
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --remove --allow-principal User:client  --operation Write  --topic 'demo_topic' 
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --remove --allow-principal User:client  --operation Read  --topic 'demo_topic' 
/usr/bin/kafka-acls --authorizer-properties zookeeper.connect=localhost:2181/kafka-dev --remove --allow-principal User:client  --operation Describe  --topic 'demo_topic' 


#In Zookeeker, how to see which brokers are connected
docker exec zk1 /usr/bin/zookeeper-shell localhost:2181 ls /kafka-dev/brokers/ids

#Where are cmds located?
/usr/bin

#How to produce?
./kafka-console-producer --topic first_topic --broker-list localhost:9092

#How to consume?
./kafka-console-consumer --topic first_topic -bootstrap-server localhost:9092

#How to list topics in zookeeper?
/usr/bin/kafka-topics --zookeeper localhost:2181 --list

#How to desc topic in zk?
./kafka-topics --zookeeper localhost:2181 --topic perf_topic --describe

#How to update retention time in zk?
./kafka-configs --zookeeper localhost:2181 --alter --entity-type topics --entity-name my-first-topic --add-config retention.ms=259200000

#How to change partitions of topic?
./kafka-topics --zookeeper zookeeper:2181 --alter --topic my-first-topic --partitions 5

#How to run performance test for producer?
./kafka-producer-perf-test --topic perf_topic --num-records 50 --throughput 10 --producer-props bootstrap.servers=localhost:9092 key.serializer=org.apache.kafka.common.serialization.StringSerializer value.serializer=org.apache.kafka.common.serialization.StringSerializer --record-size 1

#How to get kafka-cluster id?
docker exec zk1 /usr/bin/zookeeper-shell localhost:2181 get /kafka-dev/cluster/id
Output: {"version":"1","id":"9gBK6-MES0Wnt-AaLJQnJQ"}
here, 9gBK6-MES0Wnt-AaLJQnJQ

#How to get Bearer token from mds?
curl --cacert ca-cert --key mdsadmin_private_key.pem --cert ca-signed-mdsadmin -u MdsAdmin:cde34rfv -s https://localhost:8090/security/1.0/authenticate

#How to assign SystemAdmin role to user schemaregistry?
curl --cacert ca-cert --key mdsadmin_private_key.pem --cert ca-signed-mdsadmin -X POST https://localhost:8090/security/1.0/principals/User:schemaregistry/roles/SecurityAdmin -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6bnVsbH0.eyJqdGkiOiI2Z1VWWk1keVNUa0c3MDJsZzhHOVFnIiwiaXNzIjoiQ29uZmx1ZW50Iiwic3ViIjoiTWRzQWRtaW4iLCJleHAiOjE1OTU3ODQxMzAsImlhdCI6MTU5NTc4MDUzMCwibmJmIjoxNTk1NzgwNDcwLCJhenAiOiJNZHNBZG1pbiIsImF1dGhfdGltZSI6MTU5NTc4MDUzMH0.kFUsvUZHzZsCjf0Gf3oge44aGL2LWKLvrtK74LsEuVzzIHqlXss925krmyVTCOefDYgSJJQ5RV5y5vP2k8kaZo6bN3925Fm0Bv-OkhOZ5Ep20-bDiTOOLQQUgeXSmSZjvPNK4Nn1tN2N8ncCvBVuEkOc71rmB7nGWjxTUh3_9c5XOV37xxJbVCbUSacXd5VcjB778W2tTWZ_1sPhuiOabc5lR6eo15onuHJYaOnGsdQhYLafjR_b-efa_p1WYLYEgHwWucvcm621vvj3_K7cNYGfYRmShRNXlE_qYb0AmPULr9Az42pGinV_9Zp7gAerlwew48g_kHppL6Bf-V1cMA" -H "Content-Type: application/json" -d '{"clusters":{"kafka-cluster":"9gBK6-MES0Wnt-AaLJQnJQ","schema-registry-cluster":"schema-registry"}}'


#How to print certificates from jks file?
keytool -v -list -keystore kafka.server.keystore.jks
or
keytool -v -list -keystore kafka.server.keystore.jks -alias server

#How to search user in ldap?
ldapsearch -x -h localhost -p 389 cn=parthproducer -b 'dc=vishtech,dc=in'

#How to search group in ldap?
ldapsearch -x -h localhost -p 389 cn=vt_demo_write -b 'dc=vishtech,dc=in'

#How do we know if SR has registered new schema-registry
In logs, we can see:
[2020-08-23 09:52:20,848] INFO Registering new schema: subject transactions-value, version null, id null, type null (io.confluent.kafka.schemaregistry.rest.resources.SubjectVersionsResource)

#How to get all subjects from Schema Registry
curl --cacert ca-cert --key mdsadmin_private_key.pem --cert ca-signed-mdsadmin -u mdsadmin:cde34rfv -s https://localhost:8081/subjects

#How to post subject to Schema Registry using token of user (parthproducer)
curl -v --cacert ca-cert --key mdsadmin_private_key.pem --cert ca-signed-mdsadmin -X POST https://localhost:8081/subjects/transactions-value/versions -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6bnVsbH0.eyJqdGkiOiI3dEhLWHJ0aVl4aDlfVGdMWlJyVzFnIiwiaXNzIjoiQ29uZmx1ZW50Iiwic3ViIjoicGFydGhwcm9kdWNlciIsImV4cCI6MTU5ODE5MzEwNywiaWF0IjoxNTk4MTg5NTA3LCJuYmYiOjE1OTgxODk0NDcsImF6cCI6InBhcnRocHJvZHVjZXIiLCJhdXRoX3RpbWUiOjE1OTgxODk1MDd9.Cw1T8Tkw4I5z40ax4IHg35OHSKIh_uEmeQvr3jMSgDmECk3Oq-JdgdKxrzeS-sMF80LG_l1xFjcbPY0xTZmdHWfm47zp-3yEexss84dQVm1sambhxi99QZRaU0vLI-7DsRjcAO3dDACrn89WwXQFABwOA-FFa0kwH1AwA81dgbDJEN07b44uV7x4MT84dcT3Cy3rlrhbQ7Gr3ZE4-RojJsdLriWfxMFSApWhsmhYHyaw0ffPIO6LqudE6uv-L9fJrY-bNHCkVQ4Wqv17cxtvj6v8weKOjtV4WCyyysBxjCPZLCUfZHRASEh6aDgrrjJBFKdRdxeWRKsYT4I9b_cOpA" -H "Content-Type: application/json" -d '{"schema":"{\"type\":\"record\",\"name\":\"Payment\",\"namespace\":\"com.vishtech.app\",\"fields\":[{\"name\":\"id\",\"type\":\"string\"},{\"name\":\"amount\",\"type\":\"double\"}]}"}'

#What we see in SR logs when we try to post new schema
[2020-08-23 13:53:46,097] INFO Authorizing actions [Action(scope='Scope(path='[]', clusters='{kafka-cluster=QnZfNO7iTDa9_uIwwsBsCg, schema-registry-cluster=schema-registry}')', resourcePattern='Subject:LITERAL:transactions-value', operation='Write', resourceReferenceCount='1', logIfAllowed='true', logIfDenied='true')] with principal parthproducer (io.confluent.kafka.schemaregistry.security.authorizer.rbac.RbacAuthorizer)


