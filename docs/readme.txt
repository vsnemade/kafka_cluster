We have 3 setups.
1. Using docker-compose
This is working properly.

2. Single Node Setup.
This is working properly. Topics get persisted. You need to create 'data' dir for kafka and 'data' and 'log' dirs for zookeeper.

3. Multi Node Setup.
This is working properly. We have setup AclAuthoriser and super user 'localhost' is working successfully. Producer and Consumer clients were not able to produce/consume if no acl provided in ZK.
